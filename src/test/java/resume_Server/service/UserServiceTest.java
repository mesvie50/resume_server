package resume_Server.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import resume_Server.domain.UserRepository;
import resume_Server.domain.User;
import resume_Server.dto.UserParam;


import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.slf4j.LoggerFactory.getLogger;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    private static final Logger log = getLogger(UserServiceTest.class);
    private UserParam userParam = new UserParam("a", "resian", "1");

    @Before
    public void input(){
        when(userRepository.findByUserId(userParam._toUser().getUserId())).thenReturn(Optional.of(userParam._toUser()));
    }

    @Test
    public void join() throws NoSuchAlgorithmException {
        String userId = "a";
        String password = "30428cc4ab489f947e144b3c459b8285";

//        when(userService.join(userParam)).thenReturn('ㅠ';;);

    }

    @Test
    public void 형변환() throws NoSuchAlgorithmException{
        log.debug("형변환 확인:{}", userService.castPassword(userParam));
    }

    @Test
    public void login(){
        String userId = "a";
        String password = "1";
        assertThat(userService.login(userId, password)).isEqualTo(true);
    }

    @Test
    public void update(){
        HttpSession session = mock(HttpSession.class);

        UserParam updateParam = new UserParam("a", "test", "123");
//        assertThat(userService.update(updateParam, session)).isEqualTo(userService.findByUserId(userParam.getUserId()));
    }

    @Test
    public void delete(){

    }

    @Test
    public void blackList(){

    }

    @Test
    public void dormancy(){

    }

    @Test
    public void dormancyRelease(){

    }

}