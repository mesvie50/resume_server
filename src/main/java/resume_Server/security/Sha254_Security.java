package resume_Server.security;

import org.slf4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static org.slf4j.LoggerFactory.getLogger;

public class Sha254_Security {

    private static final Logger log = getLogger(Sha254_Security.class);

    private static byte[] castPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(password.getBytes());
        log.debug("sha확인:{}", messageDigest.digest());

        return messageDigest.digest();
    }

    public static String passwordSecurity(String password) throws NoSuchAlgorithmException {
        StringBuffer stringBuffer = new StringBuffer();
        byte[] castPassword = castPassword(password);

        for (int i = 0; i < castPassword.length; i++) {
            byte passwordByte = castPassword[i];
            String inputPassword = Integer.toString((passwordByte & 0xff) + 0 * 100, 16).substring(1);

            stringBuffer.append(inputPassword);
        }
        return stringBuffer.toString();
    }
}