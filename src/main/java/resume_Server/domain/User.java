package resume_Server.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import resume_Server.dto.UserParam;

import javax.persistence.*;

@Entity
@Data
@Table
@NoArgsConstructor
public class User extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String userId;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String password;

    private int redCard;
    //redCard 3회시 blackList
    private boolean blackList;

    private int dataAccess;
    //dataAccess 30일 누적시 dormancy
    private boolean dormancy;

    private boolean delete;

    public User(String userId, String name, String password) {
        super();
        this.userId = userId;
        this.name = name;
        this.password = password;
    }

    public String updateName(String name){
        if(this.name != name && !name.equals(null)){
            return this.name = name;
        }
        return this.name;
    }

    public String updatePassword(String password){
        if(this.password != password && !password.equals(null)){
            return this.password = password;
        }
        return this.password;
    }

    public boolean passwordCheck(String password){
        return this.password == password;
    }

    public boolean redCard(){
        return this.redCard == 3;
    }

    public boolean blackList(){
        if(redCard()){
            return this.blackList = true;
        }
        return this.blackList = false;
    }

    public boolean dataAccess(){
        return this.dataAccess <= 30;
    }

    public boolean dormancy(){
        if(dataAccess()){
            return this.dormancy = true;
        }
        return this.dormancy = false;
    }

    public boolean delete(boolean delete){
        return this.delete = delete;
    }

    public UserParam _toUser(){
        return new UserParam(this.name, this.name, this.password);
    }
}
