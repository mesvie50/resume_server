package resume_Server.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import resume_Server.domain.User;

import javax.persistence.Column;

@Data
@NoArgsConstructor
public class UserParam {

    @Column(nullable = false, unique = true)
    private String userId;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String password;

    public UserParam(String userId, String name, String password) {
        this.userId = userId;
        this.name = name;
        this.password = password;
    }

    public User _toUser(){
        return new User(this.userId, this.name, this.password);
    }
}
