package resume_Server;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import resume_Server.domain.User;
import resume_Server.dto.UserParam;
import resume_Server.service.UserService;

@Component
public class Mutation implements GraphQLMutationResolver {

    @Autowired
    private UserService userService;

    public User createUser(UserParam user){
        return userService.save(user);
    }

}
