package resume_Server;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import resume_Server.domain.User;
import resume_Server.service.UserService;

import java.util.List;
import java.util.Optional;

@Component
public class Query implements GraphQLQueryResolver {

    @Autowired
    private UserService userService;

    public List<User> findAll(){
        return userService.findAll();
    }

    public Optional<User> findByName(String userId){
        return userService.findByUserId(userId);
    }

}
