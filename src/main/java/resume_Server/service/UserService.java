package resume_Server.service;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import resume_Server.domain.User;
import resume_Server.domain.UserRepository;
import resume_Server.dto.UserParam;
import resume_Server.security.Sha254_Security;

import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    private String SESSION_USER = "sessiondUser";

    private static final Logger log = getLogger(UserService.class);

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public Optional<User> findByUserId(String userId){
        return userRepository.findByUserId(userId);
    }

    public User save(UserParam userParam){
        return userRepository.save(userParam._toUser());
    }

    public boolean login(String userId, String password) {
        Optional<User> maybeUser = userRepository.findByUserId(userId);
        return maybeUser.isPresent() && maybeUser.get().passwordCheck(password);
    }

    public void logout(HttpSession session){
        session.removeAttribute(SESSION_USER);
    }

    public UserParam castPassword(UserParam userParam) throws NoSuchAlgorithmException {
        String securityPassword = Sha254_Security.passwordSecurity(userParam.getPassword());
        userParam.setPassword(securityPassword);
        return userParam;
    }

    public User join(UserParam userParam) throws NoSuchAlgorithmException{
        UserParam securityPassword = castPassword(userParam);

        log.debug("aa:{}", securityPassword);

        log.debug("aa.User:{}", securityPassword._toUser());
        return save(securityPassword);
    }

    private User updateUser(UserParam userParam){
        Optional<User> maybeUser = findByUserId(userParam.getUserId());

        if(maybeUser.isPresent()){
            maybeUser.get().updateName(userParam.getName());
            maybeUser.get().updatePassword(userParam.getPassword());
        }
        return maybeUser.orElseThrow(null);
    }

    public User update(UserParam userParam, HttpSession session){
        session.setAttribute(SESSION_USER, updateUser(userParam));
        return userRepository.save(updateUser(userParam));
    }

    public User delete(HttpSession session){
        User sessiondUser = (User)session.getAttribute(SESSION_USER);
        Optional<User> maybeUser = findByUserId(sessiondUser.getUserId());

        if(maybeUser.isPresent()){
            maybeUser.get().delete(true);
        }
        return userRepository.save(maybeUser.orElseThrow(null));
    }
}
